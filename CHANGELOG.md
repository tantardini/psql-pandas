# Changelog

<!--next-version-placeholder-->

## v1.6.4 (2024-03-13)

### Fix

- allowing to use python3.9

## v1.6.3 (2024-01-26)

### Fix

- to force release: allowing only case-insensitive colnames, not spaces and reserved keywords

## v1.6.2 (2024-01-26)

### Fix

- ci

## v1.6.1 (2024-01-26)

### Fix

- updating versions

## v1.6.0 (2024-01-25)

### Feat

- escaping spaces in colnames while creating query and inserting values

## v1.5.3 (2023-09-26)

### Fix

- **tables**: handling lack of schema keys in tables info

## v1.5.2 (2023-09-26)

### Fix

- **tables**: getting schema from tables info

## v1.5.1 (2023-09-26)

### Fix

- **tables**: fixed formatting

## v1.5.0 (2023-09-26)

### Feat

- **tables**: allowing helper functions to use schema

## v1.4.0 (2023-09-25)

### Feat

- **postgresql**: supporto to specify schema in loading table

## v1.3.2 (2023-07-17)

### Fix

- **postgresql**: explicitly added usage of sqlalchemy

## v1.3.1 (2023-07-17)

### Fix

- changed comment
- **pyproject**: adding required dependency: sqlalchemy

## v1.3.0 (2023-07-14)

### Feature

* **postgresql:** Added support for bind parameters while reading data ([`fc44b81`](https://gitlab.com/tantardini/psql-pandas/-/commit/fc44b811ad5b87655f5acb313589004efcebf8dc))

## v1.2.4 (2023-07-11)

### Fix

* **pyproject:** Allowing older version of numpy ([`d167db6`](https://gitlab.com/tantardini/psql-pandas/-/commit/d167db6cc104765393f703a6c7d9c7711920cdbc))

## v1.2.3 (2023-07-05)

### Fix

* **postgresql:** Changed read method ([`f84b0b3`](https://gitlab.com/tantardini/psql-pandas/-/commit/f84b0b37833ce148899fa11f6b00e22ce5170b53))

## v1.2.2 (2023-07-05)

### Fix

* **tables:** Added explicit return if all checks are ok ([`3916dd7`](https://gitlab.com/tantardini/psql-pandas/-/commit/3916dd7def662d0799234954f66022197f2620b9))

## v1.2.1 (2023-07-05)

### Fix

* **tables:** Added missing argument to function ([`152a7db`](https://gitlab.com/tantardini/psql-pandas/-/commit/152a7dbed01baf118cdb9834d4cb44769dc30817))

## v1.2.0 (2023-07-04)

### Feature

* **tables:** Added util function to initialize table in db according to dict of specs ([`b41b3c5`](https://gitlab.com/tantardini/psql-pandas/-/commit/b41b3c5afc0d9e8593974e275db8a5283060565f))

## v1.1.0 (2023-05-30)
### Feature

* **postgresql:** Added argument to return dataframe with uppercase colnames ([`5796152`](https://gitlab.com/tantardini/psql-pandas/-/commit/57961524fe49065441ce1df8a7dc7906b083968f))

## v1.0.0 (2023-05-29)
### Breaking

* removed old 'read_from_db' method ([`a05c1c7`](https://gitlab.com/tantardini/psql-pandas/-/commit/a05c1c7fa91d8de504c7332c5ef0cb294611a592))

## v0.0.2 (2023-05-16)
### Fix
* Updated pandas to major 2 ([`5ef2443`](https://gitlab.com/tantardini/psql-pandas/-/commit/5ef24431820433f352ff6835a3c4b4d6de8cff5e))

## v0.0.1 (2023-03-15)
### Fix
* **postgresql:** Added annotations and formatted docstrings ([`8bfe34c`](https://gitlab.com/tantardini/psql-pandas/-/commit/8bfe34c78d6838e32bb5b684230e550740281c53))

### Documentation
* **readme:** Added installation command ([`e6d1b79`](https://gitlab.com/tantardini/psql-pandas/-/commit/e6d1b790e42603d836e600ed5b009e30f81a04ad))
